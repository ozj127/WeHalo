// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  // API 调用都保持和云函数当前所在环境一致
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database()
const commentOpenids = db.collection('comment_openids')
// 云函数入口函数
exports.main = async (event, context) => {
  console.log("callback event")
  console.log(event)
  let openid = ''
  await commentOpenids.doc(String(event.commentid))
  .get().then(res=>{
    console.log(res);
    openid = res.data.openid;
  }).catch(err=>{
    console.log(err);
  });
  console.log('openid' + openid)
  if (openid == null || openid == ''){
    return null
  } else {
      const result = await cloud.openapi.subscribeMessage.send({
        touser: openid,
        page: 'pages/post/post?postId=' + event.postid,
        lang: 'zh_CN',
        data: {
          "thing1": {
            value: event.author
          },
          "time2": {
            value: event.time
          },
          "thing3": {
            value: event.posttitle
          },
          "thing4": {
            value: event.content
          },
          "thing5": {
            value: event.detail
          }
        },
        templateId: '8i2cvNca5pAxF08DBrNOc3NzBoBGUPcmgHdPAmwpTv4',
        miniprogramState: 'developer'
      })
    
      console.log(result)
    
      return result
  }
}