// miniprogram/pages/pull/pull.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  //点击订阅代码
  subscriptionNotice: function () {
    subscribe.subscription(['JRyK7rpbY7bzUn_REvgP2I6ZGV7c0HojMW64gmv2Yb4',
      'kQ3j1dYlS3on7X1X_hfZGsQWx4boY5HiIKsnqrcgUUw',
      'NxuqffSJDeu9WuiYQdwPdlLfbiajOn_kf8zeu7xpJAU'])
  }
})