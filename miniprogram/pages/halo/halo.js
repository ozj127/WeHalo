// miniprogram/pages/halo/halo.js
const app = getApp();
const request = require('../../utils/request.js');
let util = require('../../utils/util.js');
let delay;//延时器
let i = 0;
let success = false;
Page({

    /**
     * 页面的初始数据
     */
    data: {
        adminOpenid: app.globalData.adminOpenid,
        roleFlag: app.globalData.roleFlag,
        replyValue: null,
        commentList: []
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var urlCounts = app.globalData.url + '/api/admin/counts';
        var paramCounts = {};
        // @todo 后台总览数据网络请求API数据
        console.log(app.globalData.adminToken);
        request.requestGetApi(urlCounts, app.globalData.adminToken, paramCounts, this, this.successCounts, this.failCounts);
        var urlLatestComments = app.globalData.url + '/api/admin/posts/comments/latest';
        var paramLatestComments = {
            top: 20
            // status: 'AUDITING'// RECYCLE, AUDITING, PUBLISHED
        };
        // @todo 后台总览数据网络请求API数据
        request.requestGetApi(urlLatestComments, app.globalData.adminToken, paramLatestComments, this, this.successLatestComments, this.failLatestComments);
        
        console.warn(app.globalData.userInfo);

        this.numDH();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        this.setData({
            roleFlag: app.globalData.roleFlag,
            adminOpenid: app.globalData.adminOpenid,
        })
        var that = this;
        // 云函数调用
        wx.cloud.callFunction({
            // 云函数名称
            name: 'get_wx_context',
            // 传给云函数的参数
            data: {
            },
            success(res) {
                // console.log("CloudResult:", res);
                // console.log("openidCloudResult:", res.result.openid);
                that.setData({
                    openid: res.result.openid
                });
                if (res.result.openid == that.data.adminOpenid) {
                    app.globalData.roleFlag = true;
                    that.setData({
                        roleFlag: true,
                    });
                    // console.warn("你是管理员！");
                } else {
                    app.globalData.roleFlag = false;
                    that.setData({
                        roleFlag: false,
                    });
                    // console.warn("你不是管理员！");
                };
            },
            fail: err => {
            },
        })
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {
    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {
        i = 0;
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    numDH: function() {
        var that = this;
        // 取消递增变动，服务器返回值的时间无法预测
        that.setData({
            post: 'ing',
            comment: 'ing',
            visit: 'ing',
            day: 'ing',
        })
    },
    coutNum: function(e) {
        if (e > 1000 && e < 10000) {
            e = (e / 1000).toFixed(1) + 'k'
        }
        if (e > 10000) {
            e = (e / 10000).toFixed(1) + 'W'
        }
        return e
    },
    
    // ListTouch触摸开始
    ListTouchStart(e) {
        this.setData({
            ListTouchStart: e.touches[0].pageX
        })
    },

    // ListTouch计算方向
    ListTouchMove(e) {
        this.setData({
            ListTouchDirection: e.touches[0].pageX - this.data.ListTouchStart > 0 ? 'right' : 'left'
        })
    },

    // ListTouch计算滚动
    ListTouchEnd(e) {
        if (this.data.ListTouchDirection == 'left') {
            this.setData({
                modalName: e.currentTarget.dataset.target
            })
        } else {
            this.setData({
                modalName: null
            })
        }
        this.setData({
            ListTouchDirection: null
        })
    },

    showModal(e) {
        this.setData({
            modalName: e.currentTarget.dataset.target,
            commentId: e.currentTarget.dataset.commentid,
            postId: e.currentTarget.dataset.postid,
            content: e.currentTarget.dataset.content,
            postTitle: e.currentTarget.dataset.posttitle,
            textareaFlag: true,
        })
    },
    hideModal(e) {
        this.setData({
            modalName: null,
            textareaFlag: false,
        })
    },

    textareaAInput(e) {
        this.setData({
            textareaAValue: e.detail.value
        })
    },

    /**
     * 回复文本框获取值
     */
    replyValue: function(e) {
        this.setData({
            replyValue: e.detail.value
        })
        console.warn(e.detail.value)
    },

    reply: function(e) {

        var that = this;
        var time = util.formatTime(new Date());
        wx.cloud.callFunction({
            // 要调用的云函数名称
            name: 'callback',
            // 传递给云函数的参数
        
            data: {
              commentid: e.currentTarget.dataset.commentid,
              postid: e.currentTarget.dataset.postid,
              author: "欧志杰",
              time: time,
              posttitle: e.currentTarget.dataset.posttitle,
              content: '作者回复了你的评论~',
              detail: "你在"+e.currentTarget.dataset.posttitle+'的评论'
            },
            success: res => {
              console.log(res)
            },
            fail: err => {
              console.log(err)
            },
          })

        setTimeout(function () {
            if(that.data.replyValue != null && that.data.replyValue != '') {

                that.setData({
                    modalName: null,
                    textareaFlag: false,
                })

                var urlReply = app.globalData.url + '/api/admin/posts/comments';
                var paramReply = {
                    author: app.globalData.userInfo.nickName,
                    authorUrl: app.globalData.userInfo.avatarUrl,
                    content: that.data.replyValue,
                    email: "1145790078@qq.com",
                    parentId: e.currentTarget.dataset.commentid,
                    postId: e.currentTarget.dataset.postid,
                };
                // @todo 后台总览数据网络请求API数据
            request.requestPostApi(urlReply, app.globalData.adminToken, paramReply, that, that.successReply, that.failReply);

                console.warn("reply" + that.data.replyValue)

            }else {
                wx.showToast({
                    title: '内容不能为空',
                    icon: 'none',
                    duration: 2000,
                    mask: true
                })
            }   

        }, 100)
    },

    /**
     * 显示回收模块
     */
    showModalrecycle(e){
        this.setData({
            modalName: e.currentTarget.dataset.target,
            commentid: e.currentTarget.dataset.commentid
        })
    },

    /**
     * 回收评论
     */
    recycle: function(res, selfObj){
        var that = this;

        setTimeout(function () {
            that.setData({
                modalName: null,
                textareaFlag: false,
            })
            var urlRecycle = app.globalData.url + '/api/admin/posts/comments/' + that.data.commentid + '/status/RECYCLE';
            // @todo 后台总览数据网络请求API数据
            request.requestPutApi(urlRecycle, app.globalData.adminToken, null, that, that.successRecycle, that.failRecycle);
            console.warn("recycle" + that.data.commentid)
        }, 100)
    },

    /**
     * 显示还原模块
     */
    showModalresume: function(e){
        this.setData({
            modalName: e.currentTarget.dataset.target,
            commentid: e.currentTarget.dataset.commentid
        })
    },

 /**
     * 还原评论
     */
    resume: function(res, selfObj){
        var that = this;

        setTimeout(function () {
            that.setData({
                modalName: null,
                textareaFlag: false,
            })
            var urlResume = app.globalData.url + '/api/admin/posts/comments/' + that.data.commentid + '/status/AUDITING';
            // @todo 后台总览数据网络请求API数据
            request.requestPutApi(urlResume, app.globalData.adminToken, null, that, that.successResume, that.failResume);
            console.warn("auditing" + that.data.commentid)
        }, 100)
    },

    /**
     * 显示发布模块
     */
    showModalpublish: function(e){
        this.setData({
            modalName: e.currentTarget.dataset.target,
            commentId: e.currentTarget.dataset.commentid,
            postId: e.currentTarget.dataset.postid,
            content: e.currentTarget.dataset.content,
            postTitle: e.currentTarget.dataset.posttitle
        })
    },

    /**
     * 发布评论
     */
    publish: function(e){
        var that = this;
        var time = util.formatTime(new Date());
        wx.cloud.callFunction({
            // 要调用的云函数名称
            name: 'callback',
            // 传递给云函数的参数
            
            data: {
              commentid: e.currentTarget.dataset.commentid,
              postid: e.currentTarget.dataset.postid,
              author: "admin",
              time: time,
              posttitle: e.currentTarget.dataset.posttitle,
              content: '你的评论通过审核已经发布了，快来看看吧~',
              detail: "你在"+e.currentTarget.dataset.posttitle+'的评论'
            },
            success: res => {
              console.log(res)
              // output: res.result === 3
            },
            fail: err => {
              console.log(err)
              // handle error
            },
          })
        setTimeout(function () {
            that.setData({
                modalName: null,
                textareaFlag: false,
            })
            var urlPublish = app.globalData.url + '/api/admin/posts/comments/' + e.currentTarget.dataset.commentid + '/status/PUBLISHED';
            // @todo 后台总览数据网络请求API数据
            request.requestPutApi(urlPublish, app.globalData.adminToken, null, that, that.successPublish, that.failPublish);
            console.warn("publish" + e.currentTarget.dataset.commentid)

        }, 100)
    },

    /**
     * 显示删除模块
     */
    showModaldelete: function(e){
        this.setData({
            modalName: e.currentTarget.dataset.target,
            commentid: e.currentTarget.dataset.commentid
        })
    },
    /**
     * 删除评论
     */
    delete: function(res, selfObj){
        var that = this;

        setTimeout(function () {
            that.setData({
                modalName: null,
                textareaFlag: false,
            })
            var urlPublish = app.globalData.url + '/api/admin/posts/comments/' + that.data.commentid;
            // @todo 后台总览数据网络请求API数据
            request.requestDeteleApi(urlPublish, app.globalData.adminToken, null, that, that.successPublish, that.failPublish);
            console.warn("delete" + that.data.commentid)
        }, 100)
    },

    /**
     * 后台数据请求--接口调用成功处理
     */
    successCounts: function(res, selfObj) {
        var that = this;
        var time1 = Date.parse(new Date());
        var time2 = res.data.birthday;
        var runTime = parseInt((time1 - time2) / 1000);
        var day = Math.floor(runTime / 86400);
        that.setData({
            post: res.data.postCount,
            comment: res.data.commentCount,
            visit: res.data.visitCount,
            day: day,
        })
    },
    /**
     * 后台数据请求--接口调用失败处理
     */
    failCounts: function (res, selfObj) {
        console.error('failAdminLogin', res)
    },


    /**
     * 最新评论请求--接口调用成功处理
     */
    successLatestComments: function(res, selfObj) {
        var that = this;
        console.warn(res)
        var list = res.data;
        for (let i = 0; i < list.length; ++i) {
            list[i].createTime = util.customFormatTime(list[i].createTime, 'Y-M-D  h:m:s');
            if (list[i].isAdmin) {
                list[i].email = '';
                list[i].authorUrl = app.globalData.userInfo.avatarUrl;
            }
        }
        that.setData({
            commentList: res.data,
        })
        console.warn(that.data.commentList)
    },
    /**
     * 最新评论请求--接口调用失败处理
     */
    failLatestComments: function (res, selfObj) {
        console.error('failLatestComments', res)
    },

    
    /**
     * 回复评论请求--接口调用成功处理
     */
    successReply: function (res, selfObj) {
        var that = this;
        // console.warn(res);
        wx.showToast({
            title: '成功回复评论',
            icon: 'none',
            duration: 2000,
            mask: true
        })
        var newComment = [{
            author: res.data.author,
            authorUrl: app.globalData.userInfo.avatarUrl,
            content: res.data.content,
            createTime: util.customFormatTime(res.data.createTime, 'Y-M-D  h:m:s'),
            email: '',
            id: res.data.id,
            isAdmin: true,
            post: {
                id: that.data.postid,
                title: that.data.postTitle,
            }
        }];
        that.setData({
            replyValue: null,
            commentList: newComment.concat(that.data.commentList),
        });
    },
    /**
     * 回复评论请求--接口调用失败处理
     */
    failReply: function (res, selfObj) {
        console.error('failReply', res)
    },

     /**
     * 回收评论请求--接口调用成功处理
     */
    successRecycle: function (res, selfObj) {
        wx.showToast({
            title: '成功回收评论',
            icon: 'none',
            duration: 2000,
            mask: true
        })
        this.onLoad();
    },
    /**
     * 回收评论请求--接口调用失败处理
     */
    failRecycle: function (res, selfObj) {
        console.error('failRecycle', res)
    },
     /**
     * 还原评论请求--接口调用成功处理
     */
    successResume: function (res, selfObj) {
        wx.showToast({
            title: '成功还原评论',
            icon: 'none',
            duration: 2000,
            mask: true
        })
        this.onLoad();
    },
    /**
     * 还原评论请求--接口调用失败处理
     */
    failResume: function (res, selfObj) {
        console.error('failResume', res)
    },
    /**
     * 发布评论请求--接口调用成功处理
     */
    successPublish: function (res, selfObj) {
        wx.showToast({
            title: '成功发布评论',
            icon: 'none',
            duration: 2000,
            mask: true
        })
        this.onLoad();
    },
    /**
     * 发布评论请求--接口调用失败处理
     */
    failPublish: function (res, selfObj) {
        console.error('failPublish', res)
    },
    /**
     * 删除评论请求--接口调用成功处理
     */
    successDelete: function (res, selfObj) {
        wx.showToast({
            title: '成功删除评论',
            icon: 'none',
            duration: 2000,
            mask: true
        })
        this.onLoad();
    },
    /**
     * 删除评论请求--接口调用失败处理
     */
    failDelete: function (res, selfObj) {
        console.error('failDelete', res)
    },
})